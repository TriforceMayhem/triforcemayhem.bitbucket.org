// the me from quite a while ago wasn't very smart.

// Older version of IE have a bad version of document.getElementById, so we make our own version.
// https://www.sixteensmallstones.org/ie-javascript-bugs-overriding-internet-explorers-documentgetelementbyid-to-be-w3c-compliant-exposes-an-additional-bug-in-getattributes/
if (navigator.userAgent.indexOf("msie") > -1){
  document.nativeGetElementById = document.getElementById;
  document.getElementById = function(id){
    var el = document.nativeGetElementById(id);
    if (el){
      if (el.attributes.id.value == id){
        return el;
      }else{
        for (var i = 1; i < document.all[id].length; i++){
          if (document.all[id][i].attributes.id.value == id){
            return document.all[id][i];
          }
        }
      }
    }
    return null;
  };
}

function App(){
  
  /* Stuff that makes the site work. */
  
  try{
    console.log("console exists");
    this.Log = console.log;
  }catch(e){
    this.Log = function(){};
  }

  this.init = function(){
    cn = 1;
    titlel = [];
    pidl = [];
    instructionsl = [];
    creditsl = [];
    oldtitle = document.title;
    location.hash = "#";
  };
  
  this.Back = function(){
    site.Log("back");
    player = document.getElementById("player");
    while (player.firstChild){
      player.removeChild(player.firstChild);
    }
    document.getElementById("list").style.display = "inline";
    document.getElementById("player").style.display = "none";
  };
  
  this.addProject = function(title, pid, instructions, credits){
    site.Log("project " + pid);
    titlel.push(title);
    pidl.push(pid);
    instructionsl.push(instructions);
    creditsl.push(credits);
    el = document.createElement("a");
    el.href = "#/project/" + cn + "/";
    el.appendChild(document.createTextNode(title));
    document.getElementById("list").appendChild(el);
    document.getElementById("list").appendChild(document.createElement("br"));
    document.getElementById("list").appendChild(document.createElement("br"));
    cn++;
  };
  
  this.embedProject = function(title, pid, instructions, credits){
    
    window.pn = location.hash.split("/")[2];
    window.pid = pid;
    
    site.Log("embed " + pid);
    document.getElementById("list").style.display = "none";
    document.getElementById("player").style.display = "inline";

    el = document.createElement("h2");
    el.appendChild(document.createTextNode(title));
    el.id = "ptitle";
    document.getElementById("player").appendChild(el);
    
    el = document.createElement("span");
    el.id = "iholder";
    document.getElementById("player").appendChild(el);
    
    el = document.createElement("iframe");
    el.id = "iframe";
    el.src = "https://scratch.mit.edu/projects/embed/" + pid + "/?autostart=false";
    el.style.border = "";
    el.width = "485";
    el.height = "402";
    el.allowTransparency = "true";
    document.getElementById("iholder").appendChild(el);
    document.getElementById("iframe").setAttribute("frameborder", "0");
    
    document.getElementById("player").appendChild(document.createElement("br"));
    
    el = document.createElement("span");
    el.id = "notes";
    document.getElementById("player").appendChild(el);
    
    if (instructions.search(/\n|(wiki\.)scratch\.mit\.edu|( @)|^@/ig) > -1){
      site.embedDisplay(instructions);
    }else{
      document.getElementById("notes").appendChild(document.createTextNode(instructions));
    }
    
    el = document.createElement("hr");
    document.getElementById("notes").appendChild(el);
    
    if (credits.search(/\n|(wiki\.)scratch\.mit\.edu|( @)|^@/ig) > -1){
      site.embedDisplay(credits);
    }else{
      document.getElementById("notes").appendChild(document.createTextNode(credits));
    }

    document.getElementById("player").appendChild(document.createElement("br"));
    document.getElementById("player").appendChild(document.createElement("br"));
    
    el = document.createElement("a");
    el.appendChild(document.createTextNode("Project"));
    el.href = "https://scratch.mit.edu/projects/" + pid + "/";
    el.target = "_blank";
    document.getElementById("player").appendChild(el);
    
    document.getElementById("player").appendChild(document.createTextNode(" | "));
    
    el = document.createElement("span");
    el.id = "ffholder";
    document.getElementById("player").appendChild(el);
    
    el = document.createElement("a");
    el.appendChild(document.createTextNode("Fullscreen"));
    el.href = location.hash + "fullscreen";
    el.id = "ff";
    document.getElementById("ffholder").appendChild(el);
    
    document.getElementById("ffholder").appendChild(document.createTextNode(" | "));
    
    el = document.createElement("a");
    el.appendChild(document.createTextNode("Phosphorus"));
    el.href = location.hash + "js";
    el.id = "js";
    document.getElementById("player").appendChild(el);
    
    document.getElementById("player").appendChild(document.createTextNode(" | "));
    
    el = document.createElement("a");
    el.appendChild(document.createTextNode("Back"));
    el.href = "#";
    document.getElementById("player").appendChild(el);

    el = document.createElement("iframe");
  };
  
  this.embedDisplay = function(t){
    site.Log("embeddisplay " + t);
    l = t.split(" ");
    for (i = 0; i < l.length; i++){
      if (l[i].search(/^@/g) > -1){
        el = document.createElement("a");
        user = /([@_\w])+/g.exec(l[i]);
        el.appendChild(document.createTextNode(l[i]));
        el.href = "https://scratch.mit.edu/users/" + user[0].split("@")[1].split("'")[0] + "/";
        el.target = "_blank";
        document.getElementById("notes").appendChild(el);
        document.getElementById("notes").appendChild(document.createTextNode(" "));
      }else if (l[i].search(/https?:\/\/(wiki\.)?scratch\.mit\.edu/gi) > -1){
        el = document.createElement("a");
        url = /https?:\/\/(wiki\.)?scratch\.mit\.edu[^ ]*/gi.exec(l[i]);
        el.appendChild(document.createTextNode(l[i]));
        el.href = url[0];
        el.target = "_blank";
        document.getElementById("notes").appendChild(el);
        document.getElementById("notes").appendChild(document.createTextNode(" "));
      }else if (l[i] === "\n"){
        document.getElementById("notes").appendChild(document.createElement("br"));
      }else{
        document.getElementById("notes").appendChild(document.createTextNode(l[i] + " "));
      }
    }
  };
  
  this.Phosphorus = function(){
    site.Log("js");
    iholder = document.getElementById("iholder");
    while (iholder.firstChild){
      iholder.removeChild(iholder.firstChild);
    }
    el = document.createElement("script");
    el.src = "https://phosphorus.github.io/embed.js?id=" + pid + "&auto-start=false";
    document.getElementById("iholder").appendChild(el);
    document.getElementById("js").href = "#/project/" + pn + "/flash";
    document.getElementById("js").innerHTML = "Flash";
    document.getElementById("ptitle").style.display = "block";
    document.getElementById("notes").style.display = "inline";
    document.getElementById("ff").href = "#/project/" + pn + "/fullscreen";
    document.getElementById("ff").innerHTML = "Large mode";
    document.getElementById("ffholder").style.display = "none";
  };
  
  this.unPhosphorus = function(){
    site.Log("flash");
    iholder = document.getElementById("iholder");
    while (iholder.firstChild){
      iholder.removeChild(iholder.firstChild);
    }
    el = document.createElement("iframe");
    el.id = "iframe";
    el.src = "https://scratch.mit.edu/projects/embed/" + pid + "/?autostart=false";
    el.style.border = "";
    el.width = "485";
    el.height = "402";
    el.allowTransparency = "true";
    document.getElementById("iholder").appendChild(el);
    document.getElementById("iframe").setAttribute("frameborder", "0");
    document.getElementById("js").href = "#/project/" + pn + "/js";
    document.getElementById("js").innerHTML = "Phosphorus";
    document.getElementById("ptitle").style.display = "block";
    document.getElementById("notes").style.display = "inline";
    document.getElementById("ff").href = "#/project/" + pn + "/fullscreen";
    document.getElementById("ff").innerHTML = "Large mode";
    document.getElementById("ffholder").style.display = "inline";
  }; 
  
  this.Fullscreen = function(){
    site.Log("ff");
    if (document.getElementById("iframe")){
      document.getElementById("iframe").width = "970px";
      document.getElementById("iframe").height = "804px";
    }else{
      document.getElementsByClassName("phosphorus")[0].width = "964";
      document.getElementsByClassName("phosphorus")[0].height = "786";
    }
    document.getElementById("ff").href = "#/project/" + pn + "/flash";
    document.getElementById("ff").innerHTML = "Normal mode";
    document.getElementById("notes").style.display = "none";
    document.getElementById("ptitle").style.display = "none";
  };
  
  this.unFullscreen = function(){
    site.Log("uff");
    if (document.getElementById("iframe")){
      document.getElementById("iframe").width = "485px";
      document.getElementById("iframe").height = "402px";
    }else{
      document.getElementsByClassName("phosphorus")[0].width = "964";
      document.getElementsByClassName("phosphorus")[0].height = "786";
    }
    document.getElementById("ff").href = "#/project/" + pn + "/fullscreen";
    document.getElementById("ff").innerHTML = "Large mode";
    document.getElementById("notes").style.display = "inline";
    document.getElementById("ptitle").style.display = "block";
  };

  window.onhashchange = function(){
    site.Log("");
    site.Log("hashchange " + location.hash);
    hash = location.hash.split("/");
    site.Log(hash);
    if (location.hash === "#" || location.hash === ""){
      site.Back();
    }else if(hash[1] === "project" && hash[3] === ""){
      id = hash[2] - 1;
      site.embedProject(titlel[id], pidl[id], instructionsl[id], creditsl[id]);
    }else if(hash[3] === "js"){
      if (document.getElementById("iframe")){
        site.Phosphorus();
      }else{
        id = hash[2] - 1;
        site.embedProject(titlel[id], pidl[id], instructionsl[id], creditsl[id]);
        site.Phosphorus();
      }
    }else if(hash[3] === "flash"){
      if (document.getElementsByClassName("phosphorus")[0]){
        site.unPhosphorus();
      }else if(document.getElementById("iframe")){
        site.unFullscreen();
      }else{
        id = hash[2] - 1;
        site.embedProject(titlel[id], pidl[id], instructionsl[id], creditsl[id]);
      }
    }else if(hash[3] === "fullscreen"){
      if (document.getElementById("iframe")){
        site.Fullscreen();
      }else{
        id = hash[2] - 1;
        site.embedProject(titlel[id], pidl[id], instructionsl[id], creditsl[id]);
        site.Fullscreen();
      }
    }
  };
}

site = new App();
site.init();

window.onload = function(){
  /* 
   * Add projets here! 
   * Use this to generate these lines:
   * https://TriforceMayhem.bitbucket.io/generate.html
   */

  site.addProject("Nightlight","150752251","Arrows - Move \n Up/Z - Jump \n Space - Pause & Options \n ","Scripting » @TriforceMayhem \n Design » @TriforceMayhem \n Inspiration » @PastryTycoon \n Music » Tom Needlr (Remixed by @TriforceMayhem), @TriforceMayhem, Nintendo");
  site.addProject("Groundroll Grahm and the Stale Memes","120104406","You are Grahm, a twitch bot made for chat monitoring. \n But as of recently, stale memes all over the internet have been lashing out, disabling twitch accounts, reddit accounts, and many more! \n Twitch has converted you into a rolling fighting robot, and you are to stop the reign of the stale memes! \n  \n Controls: In game \n Saving/Loading: Stop the game, hit [P] to get a code. Use this code by clicking on the P Tank in the start. \n E-Tanks: You have a chance to find them in a chest. To use it, hit the [D] key. This will restore your health. \n ","Programming & Design » @TriforceMayhem \n Music » Kevin MacLeod, Various YouTube Songs, Capcom, @TriforceMayhem \n Many characters, memes, and chips belong to their respective owners.");
  site.addProject("Winter Dash","142380583","Up » Jump \n Space » Start","Made for round 4 in @BobbyF's Scratch Winter Tourney");
  site.addProject("Pickle Puzzle Jar","138954217","Arrows - Move \n Z - Jump \n X - Interact","As of recently, your local pickle company has shut down! You end up going to their abandoned factory to find the last pickle jar. \n  \n There is an odd bug that causes the puzzle levels to break on some computers, I cannot reproduce it so I can't fix it. \n  \n This is my entry for round 3 of @BobbyF's SWT tourney!");
  site.addProject("Hurry to the Hype Train!", "109366611", "Arrows to move, up or z to jump.", "@oromi inspired me with his GabeN game.");
  site.addProject("Super Suits", "79038438", "Instructions are given in-game. \n \n ! ! ! KNOWN GLITCH FIX ! ! ! \n If you get stuck in a block, don't worry! \n Just click the green flag again and \n you'll be back in the last level you were at.", "Music » @TriforceMayhem @Flint-L \n Player Engine » @JamesOuO");
};
